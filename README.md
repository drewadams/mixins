### Miscellaneous SCSS Mixins ###

List of miscellaneous SCSS mixins I have found to be helpful throughout my development.

### Setup ###

You'll need to install (if you haven't already) and use an SCSS preprocessor (Gulp, Compass, Koala, etc) and link files appropriately. I like to create a separate 'mixins.scss' page to keep my styles a little more organized. Each mixin should have an 'include' comment above it describing how that particular mixin may be implemented. For more information on SCSS mixins, please refer to:
http://sass-lang.com/guide

### Contribution guidelines ###

Send a pull request - if the mixin doesn't exist and is relevant, it will be added.